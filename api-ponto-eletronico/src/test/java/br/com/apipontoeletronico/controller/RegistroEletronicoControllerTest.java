package br.com.apipontoeletronico.controller;

import br.com.apipontoeletronico.enums.TipoRegistroEnum;
import br.com.apipontoeletronico.models.DTO.PontoBatidoDTO;
import br.com.apipontoeletronico.models.RegistroEletronico;
import br.com.apipontoeletronico.models.Usuario;
import br.com.apipontoeletronico.service.RegistroEletronicoService;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@WebMvcTest(RegistroEletronicoController.class)
public class RegistroEletronicoControllerTest {

    @Autowired
    private RegistroEletronicoController registroEletronicoController;

    @MockBean
    private RegistroEletronicoService registroEletronicoService;

    @Autowired
    private MockMvc mockMvc;

    private Usuario usuario;
    private RegistroEletronico registroEletronico;
    private PontoBatidoDTO pontoBatidoDTO;

    @BeforeEach
    public void setUp() {
        usuario = new Usuario();
        usuario.setId(1);
        usuario.setDataCadastro(LocalDate.now());
        usuario.setCpf("351.039.548-42");
        usuario.setEmail("usuario@gmail.com");
        usuario.setNomeCompleto("Usuario Teste");

        Date data = new Date();
        registroEletronico = new RegistroEletronico();
        registroEletronico.setId(1);
        registroEletronico.setTipoBatida(TipoRegistroEnum.E);
        registroEletronico.setDataRegistro(data);
        registroEletronico.setUsuario(usuario);

        pontoBatidoDTO = new PontoBatidoDTO();
        pontoBatidoDTO.setTotalTrabalhado("2:56:46");
    }

    @Test
    public void testarCadastrarPontoEletronico() throws Exception {
        Mockito.when(registroEletronicoService.cadastrarPontoEletronico(Mockito.anyLong(),
                Mockito.any(TipoRegistroEnum.class))).then(retorno->{
            return registroEletronico;
        });
        mockMvc.perform(MockMvcRequestBuilders.post("/registrosEletronico/1?tipoRegistro=S"))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }
    @Test
    public void testarCadastrarPontoEletronicoComErro() throws Exception {
        Mockito.when(registroEletronicoService.cadastrarPontoEletronico(Mockito.anyLong(),
                Mockito.any(TipoRegistroEnum.class))).then(retorno->{
            throw new RuntimeException("ERRO");
        });

        mockMvc.perform(MockMvcRequestBuilders.post("/registrosEletronico/1?tipoRegistro=S"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }
    @Test
    public void testarListarPontosBatidos() throws Exception {
        Mockito.when(registroEletronicoService.listarPontosBatidosDoDia(Mockito.anyLong())).thenReturn(pontoBatidoDTO);
        mockMvc.perform(MockMvcRequestBuilders.get("/registrosEletronico/1"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.totalTrabalhado"
                        , CoreMatchers.equalTo("2:56:46")));
    }
    @Test
    public void testarListarPontosBatidosComErro() throws Exception {
        Mockito.when(registroEletronicoService.listarPontosBatidosDoDia(Mockito.anyLong())).then(retorno->{
            throw new RuntimeException("ERRO");
        });
        mockMvc.perform(MockMvcRequestBuilders.get("/registrosEletronico/1"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }
}
