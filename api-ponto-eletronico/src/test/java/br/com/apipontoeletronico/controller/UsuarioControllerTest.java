package br.com.apipontoeletronico.controller;

import br.com.apipontoeletronico.models.Usuario;
import br.com.apipontoeletronico.service.UsuarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@WebMvcTest(UsuarioController.class)
public class UsuarioControllerTest {
    @Autowired
    private UsuarioController usuarioController;
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UsuarioService usuarioService;

    private Usuario usuario;

    @BeforeEach
    public void setUp() {
        usuario = new Usuario();
        usuario.setId(1);
        usuario.setCpf("351.039.548-42");
        usuario.setEmail("usuario@gmail.com");
        usuario.setNomeCompleto("Usuario Teste");
    }

    @Test
    public void testarCadastrarUsuario() throws Exception {
        Mockito.when(usuarioService.cadastrarUsuario(Mockito.any(Usuario.class))).then(usuarioObjeto->{
            usuario.setDataCadastro(LocalDate.now());
            return usuario;
        });

        ObjectMapper mapper = new ObjectMapper(); //Conversor de Classe em Json!
        String jsonDeUsuario = mapper.writeValueAsString(usuario);

        mockMvc.perform(MockMvcRequestBuilders.post("/usuarios")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeUsuario))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.dataCadastro", CoreMatchers.equalTo(LocalDate.now().toString())));
    }
    @Test
    public void testarCadastrarUsuarioDuplicado() throws Exception {
        Mockito.when(usuarioService.cadastrarUsuario(Mockito.any(Usuario.class))).then(usuarioObjeto->{
            throw new RuntimeException("Usuario duplicado");
        });

        ObjectMapper mapper = new ObjectMapper(); //Conversor de Classe em Json!
        String jsonDeUsuario = mapper.writeValueAsString(usuario);

        mockMvc.perform(MockMvcRequestBuilders.post("/usuarios")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeUsuario))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }
    @Test
    public void testarEditarUsuarioExistente() throws Exception {
        Mockito.when(usuarioService.editarUsuario(Mockito.anyLong(),Mockito.any(Usuario.class))).then(usuarioObjeto->{
            usuario.setDataCadastro(LocalDate.now());
            return usuario;
        });

        ObjectMapper mapper = new ObjectMapper(); //Conversor de Classe em Json!
        String jsonDeUsuario = mapper.writeValueAsString(usuario);

        mockMvc.perform(MockMvcRequestBuilders.put("/usuarios/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeUsuario))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
    }
    @Test
    public void testarEditarUsuarioInexistente() throws Exception {
        Mockito.when(usuarioService.editarUsuario(Mockito.anyLong(),Mockito.any(Usuario.class))).then(usuarioObjeto->{
            throw new RuntimeException("Usuario não existente");
        });

        ObjectMapper mapper = new ObjectMapper(); //Conversor de Classe em Json!
        String jsonDeUsuario = mapper.writeValueAsString(usuario);

        mockMvc.perform(MockMvcRequestBuilders.put("/usuarios/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeUsuario))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }
    @Test
    public void testarConsultarUsuarioExistente() throws Exception {
        Mockito.when(usuarioService.consultarUsuario(Mockito.anyLong())).then(usuarioObjeto->{
            usuario.setDataCadastro(LocalDate.now());
            return usuario;
        });

        ObjectMapper mapper = new ObjectMapper(); //Conversor de Classe em Json!
        String jsonDeUsuario = mapper.writeValueAsString(usuario);

        mockMvc.perform(MockMvcRequestBuilders.get("/usuarios/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeUsuario))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
    }
    @Test
    public void testarConsultarUsuarioInexistente() throws Exception {
        Mockito.when(usuarioService.consultarUsuario(Mockito.anyLong())).then(usuarioObjeto->{
            throw new RuntimeException("Usuario não existente");
        });

        ObjectMapper mapper = new ObjectMapper(); //Conversor de Classe em Json!
        String jsonDeUsuario = mapper.writeValueAsString(usuario);

        mockMvc.perform(MockMvcRequestBuilders.get("/usuarios/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeUsuario))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }
    @Test
    public void testarConultarTodosUsuariosExistente() throws Exception {
        Mockito.when(usuarioService.consultarTodosUsuarios()).then(usuarioObjeto->{
            List<Usuario> usuarios = new ArrayList<>();
            usuarios.add(usuario);
            usuarios.add(usuario);
            return usuarios;
        });
        mockMvc.perform(MockMvcRequestBuilders.get("/usuarios"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(2));
    }
    @Test
    public void testarConultarTodosUsuariosInexistente() throws Exception {
        Mockito.when(usuarioService.consultarTodosUsuarios()).then(usuarioObjeto->{
            throw new RuntimeException();
        });
        mockMvc.perform(MockMvcRequestBuilders.get("/usuarios"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }



}
