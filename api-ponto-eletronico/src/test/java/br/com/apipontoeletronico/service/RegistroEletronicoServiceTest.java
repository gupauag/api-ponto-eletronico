package br.com.apipontoeletronico.service;

import br.com.apipontoeletronico.enums.TipoRegistroEnum;
import br.com.apipontoeletronico.models.DTO.PontoBatidoDTO;
import br.com.apipontoeletronico.models.DTO.TipoRegistroDTO;
import br.com.apipontoeletronico.models.RegistroEletronico;
import br.com.apipontoeletronico.models.Usuario;
import br.com.apipontoeletronico.repositories.RegistroEletronicoRepository;
import br.com.apipontoeletronico.repositories.UsuarioRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import javax.xml.crypto.Data;
import java.lang.reflect.Array;
import java.time.LocalDate;
import java.util.*;

@SpringBootTest
public class RegistroEletronicoServiceTest {
    @Autowired
    private RegistroEletronicoService registroEletronicoService;

    @MockBean
    private RegistroEletronicoRepository registroEletronicoRepository;
    @MockBean
    private UsuarioRepository usuarioRepository;

    private Usuario usuario;
    private RegistroEletronico registroEletronico;
    private RegistroEletronico registroEletronicoSaida;

    @BeforeEach
    public void setUp() {
        usuario = new Usuario();
        usuario.setId(1);
        usuario.setDataCadastro(LocalDate.now());
        usuario.setCpf("351.039.548-42");
        usuario.setEmail("usuario@gmail.com");
        usuario.setNomeCompleto("Usuario Teste");

        Date data = new Date();
        registroEletronico = new RegistroEletronico();
        registroEletronico.setId(1);
        registroEletronico.setTipoBatida(TipoRegistroEnum.E);
        registroEletronico.setDataRegistro(data);
        registroEletronico.setUsuario(usuario);

        Date data2 = new Date();
        registroEletronicoSaida = new RegistroEletronico();
        registroEletronicoSaida.setId(2);
        registroEletronicoSaida.setTipoBatida(TipoRegistroEnum.S);
        registroEletronicoSaida.setDataRegistro(data2);
        registroEletronicoSaida.setUsuario(usuario);
    }

    @Test
    public void testarCadastrarPontoEletronicoUsuarioExistente(){
        Mockito.when(usuarioRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(usuario));
        Mockito.when(registroEletronicoRepository.ultimoResgistroPontoDoDia(Mockito.anyLong())).thenReturn("S");
        Mockito.when(registroEletronicoRepository.save(Mockito.any(RegistroEletronico.class))).then(resultado ->{
            return registroEletronico;
        });

        RegistroEletronico registroEletronicoResultado =
                registroEletronicoService.cadastrarPontoEletronico(1, TipoRegistroEnum.E);

        Assertions.assertEquals(registroEletronicoResultado,registroEletronico);
    }
    @Test
    public void testarCadastrarPontoEletronicoUsuarioInexistente(){
        Mockito.when(usuarioRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());

        Assertions.assertThrows(RuntimeException.class,
                () -> {
                    registroEletronicoService.cadastrarPontoEletronico(1, TipoRegistroEnum.S);
                });
    }
    @Test
    public void testarCadastrarPontoEletronicoUserExistenteUltimoRegistroNull(){
        Mockito.when(usuarioRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(usuario));
        Mockito.when(registroEletronicoRepository.ultimoResgistroPontoDoDia(Mockito.anyLong())).thenReturn(null);
        Mockito.when(registroEletronicoRepository.save(Mockito.any(RegistroEletronico.class))).then(resultado ->{
            return registroEletronico;
        });

        RegistroEletronico registroEletronicoResultado =
                registroEletronicoService.cadastrarPontoEletronico(1, TipoRegistroEnum.E);

        Assertions.assertEquals(registroEletronicoResultado,registroEletronico);
    }
    @Test
    public void testarCadastrarPontoEletronicoUserExistenteUltimoRegistroSComRegistroDeEntrada(){
        Mockito.when(usuarioRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(usuario));
        Mockito.when(registroEletronicoRepository.ultimoResgistroPontoDoDia(Mockito.anyLong())).thenReturn("S");
        Mockito.when(registroEletronicoRepository.save(Mockito.any(RegistroEletronico.class))).then(resultado ->{
            return registroEletronico;
        });
        RegistroEletronico registroEletronicoResultado =
                registroEletronicoService.cadastrarPontoEletronico(1, TipoRegistroEnum.E);
        Assertions.assertEquals(registroEletronicoResultado,registroEletronico);
    }
    @Test
    public void testarCadastrarPontoEletronicoUserExistenteUltimoRegistroSComRegistroDeSaida(){
        Mockito.when(usuarioRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(usuario));
        Mockito.when(registroEletronicoRepository.ultimoResgistroPontoDoDia(Mockito.anyLong())).thenReturn("S");
        Assertions.assertThrows(RuntimeException.class,
                () -> {
                    registroEletronicoService.cadastrarPontoEletronico(1, TipoRegistroEnum.S);
                });
    }
    @Test
    public void testarCadastrarPontoEletronicoUserExistenteReturnUltimoRegistroEComRegistroDeSaida(){
        Mockito.when(usuarioRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(usuario));
        Mockito.when(registroEletronicoRepository.ultimoResgistroPontoDoDia(Mockito.anyLong())).thenReturn("E");
        Mockito.when(registroEletronicoRepository.save(Mockito.any(RegistroEletronico.class))).then(resultado ->{
            return registroEletronico;
        });
        RegistroEletronico registroEletronicoResultado =
                registroEletronicoService.cadastrarPontoEletronico(1, TipoRegistroEnum.S);
        Assertions.assertEquals(registroEletronicoResultado,registroEletronico);
    }
    @Test
    public void testarCadastrarPontoEletronicoUserExistenteReturnUltimoRegistroEComRegistroDeEntrada(){
        Mockito.when(usuarioRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(usuario));
        Mockito.when(registroEletronicoRepository.ultimoResgistroPontoDoDia(Mockito.anyLong())).thenReturn("E");
        Assertions.assertThrows(RuntimeException.class,
                () -> {
                    registroEletronicoService.cadastrarPontoEletronico(1, TipoRegistroEnum.E);
                });
    }

    @Test
    public void testarListarPontosBatidosUsuarioExistente(){

        Mockito.when(registroEletronicoRepository.findByUsuarioId(Mockito.anyLong()))
                .thenReturn(Arrays.asList(registroEletronico));

        List<RegistroEletronico> lista = new ArrayList<>();
        lista.add(registroEletronico);
        lista.add(registroEletronicoSaida);
        lista.add(registroEletronico);

        Mockito.when(registroEletronicoRepository.getTodosRegistrosDoDia(Mockito.anyLong()))
                .thenReturn(lista);

        List<TipoRegistroDTO> listaTipoRegistroDTOS = new ArrayList<>();
        TipoRegistroDTO tipoRegistroDTO = new  TipoRegistroDTO();
        tipoRegistroDTO.setDataRegistro(registroEletronico.getDataRegistro());
        tipoRegistroDTO.setTipoBatida(registroEletronico.getTipoBatida());
        listaTipoRegistroDTOS.add(tipoRegistroDTO);

        PontoBatidoDTO pontoBatidoDTO = new PontoBatidoDTO();
        pontoBatidoDTO.setTotalTrabalhado("");
        pontoBatidoDTO.setRegistrosPonto(listaTipoRegistroDTOS);

        PontoBatidoDTO pontoBatidoDTORetorno = registroEletronicoService.listarPontosBatidosDoDia(1);
        Assertions.assertEquals(pontoBatidoDTORetorno.getRegistrosPonto().get(0).getDataRegistro(),pontoBatidoDTO.getRegistrosPonto().get(0).getDataRegistro());
    }

    @Test
    public void testarListarPontosBatidosUsuarioInexistente(){

        Mockito.when(registroEletronicoRepository.findByUsuarioId(Mockito.anyLong())).then(retorno->{
            List<RegistroEletronico> lista = new ArrayList<>();
            return lista;
        });

        Assertions.assertThrows(RuntimeException.class,
                () -> {
                    registroEletronicoService.listarPontosBatidosDoDia(1);
                });
    }
}

