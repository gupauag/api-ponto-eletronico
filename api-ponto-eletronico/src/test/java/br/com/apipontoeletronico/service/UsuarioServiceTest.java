package br.com.apipontoeletronico.service;

import br.com.apipontoeletronico.models.Usuario;
import br.com.apipontoeletronico.repositories.UsuarioRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

@SpringBootTest
public class UsuarioServiceTest {

    @Autowired
    private UsuarioService usuarioService;

    @MockBean
    private UsuarioRepository usuarioRepository;

    private Usuario usuario;

    @BeforeEach
    public void setUp() {
        usuario = new Usuario();
        usuario.setDataCadastro(LocalDate.now());
        usuario.setCpf("351.039.548-42");
        usuario.setEmail("usuario@gmail.com");
        usuario.setNomeCompleto("Usuario Teste");
    }
    @Test
    public void testarCadastrarUsuarioNovo(){
        Mockito.when(usuarioRepository.save(Mockito.any(Usuario.class))).thenReturn(usuario);

        Usuario usuarioRetorno = usuarioService.cadastrarUsuario(usuario);
        Assertions.assertEquals(usuarioRetorno,usuario);
    }
    @Test
    public void testarCadastrarUsuarioEmailExistente(){
        Mockito.when(usuarioRepository.findByEmail(Mockito.anyString())).thenReturn(null);

        Assertions.assertThrows(RuntimeException.class,
                () -> {
                    usuarioService.cadastrarUsuario(usuario);
                });
    }
    @Test
    public void testarEditarUsuarioExistente(){
        Mockito.when(usuarioRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(usuario));
        Mockito.when(usuarioRepository.save(Mockito.any(Usuario.class))).thenReturn(usuario);

        Usuario usuarioRetorno = usuarioService.editarUsuario(1,usuario);

        Assertions.assertEquals(usuarioRetorno, usuario);
    }
    @Test
    public void testarEditarUsuarioInexistente(){
        Mockito.when(usuarioRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());
        Assertions.assertThrows(RuntimeException.class,
                () -> {
                    usuarioService.editarUsuario(1,usuario);
                });
    }

    @Test
    public void testarConsultarUsuarioExistente(){
        Mockito.when(usuarioRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(usuario));

        Usuario usuarioRetorno = usuarioService.consultarUsuario(1);
        Assertions.assertEquals(usuarioRetorno,usuario);
    }
    @Test
    public void testarConsultarUsuarioInexistente(){
        Mockito.when(usuarioRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());

        Assertions.assertThrows(RuntimeException.class,
                () -> {
                    usuarioService.consultarUsuario(1);
                });
    }
    @Test
    public void testarConsultarTodosUsuarios(){
        Mockito.when(usuarioRepository.findAll()).thenReturn(Arrays.asList(usuario));
        Iterable<Usuario> usuarios = usuarioService.consultarTodosUsuarios();
        Assertions.assertEquals(usuarios,Arrays.asList(usuario));
    }
    @Test
    public void testarConsultarTodosUsuariosComErro(){
        Mockito.when(usuarioRepository.findAll()).then(usuarioObjeto -> {
            throw new Exception();
        });
        Assertions.assertThrows(RuntimeException.class,
                () -> {
                    usuarioService.consultarTodosUsuarios();
                });
    }


//    public Iterable<Usuario> consultarTodosUsuarios(){
//        try {
//            return usuarioRepository.findAll();
//        }catch (Exception e){
//            throw new RuntimeException("Não foi possível achar usuarios");
//        }
//    }

}
