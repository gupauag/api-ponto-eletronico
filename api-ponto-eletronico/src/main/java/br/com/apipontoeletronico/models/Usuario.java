package br.com.apipontoeletronico.models;

import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Entity
public class Usuario {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private long id;

    @Size(min=3, max = 255, message = "O nome deve ter entre 3 e 255 caracteres")
    @NotNull(message = "Inclusão do nome é obrigatória")
    private String nomeCompleto;

    @CPF(message = "CPF inválido!")
    @NotNull(message = "Inclusão do CPF é obrigatório")
    private String cpf;

    @Email(message = "O formato do email é inválido")
    @NotNull(message = "Inclusão do email é obrigatória")
    private String email;

    private LocalDate dataCadastro;

    public Usuario() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNomeCompleto() {
        return nomeCompleto;
    }

    public void setNomeCompleto(String nomeCompleto) {
        this.nomeCompleto = nomeCompleto;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(LocalDate dataCadastro) {
        this.dataCadastro = dataCadastro;
    }
}
