package br.com.apipontoeletronico.models.DTO;

import java.util.List;

public class PontoBatidoDTO {
    private String totalTrabalhado;
    private List<TipoRegistroDTO> registrosPonto;

    public PontoBatidoDTO() {
    }

    public String getTotalTrabalhado() {
        return totalTrabalhado;
    }

    public void setTotalTrabalhado(String totalTrabalhado) {
        this.totalTrabalhado = totalTrabalhado;
    }

    public List<TipoRegistroDTO> getRegistrosPonto() {
        return registrosPonto;
    }

    public void setRegistrosPonto(List<TipoRegistroDTO> registrosPonto) {
        this.registrosPonto = registrosPonto;
    }
}
