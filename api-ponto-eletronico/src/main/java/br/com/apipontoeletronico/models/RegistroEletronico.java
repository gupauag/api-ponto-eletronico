package br.com.apipontoeletronico.models;

import br.com.apipontoeletronico.enums.TipoRegistroEnum;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
public class RegistroEletronico {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private long id;

    private Date dataRegistro;

    @NotNull(message = "Tipo de entrada é obrigatória ")
    @Enumerated(EnumType.STRING)
    private TipoRegistroEnum tipoBatida; //S = saída | E = entrada

    @ManyToOne(cascade = CascadeType.ALL)
    @NotNull(message = "Inclusão de Usuario é obrigatória")
    private Usuario usuario;

    public RegistroEletronico() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDataRegistro() {
        return dataRegistro;
    }

    public void setDataRegistro(Date dataRegistro) {
        this.dataRegistro = dataRegistro;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public TipoRegistroEnum getTipoBatida() {
        return tipoBatida;
    }

    public void setTipoBatida(TipoRegistroEnum tipoBatida) {
        this.tipoBatida = tipoBatida;
    }

}
