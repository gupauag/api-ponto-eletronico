package br.com.apipontoeletronico.models.DTO;

import br.com.apipontoeletronico.enums.TipoRegistroEnum;

import java.util.Date;

public class TipoRegistroDTO {
    private Date dataRegistro;
    private TipoRegistroEnum tipoBatida;

    public TipoRegistroDTO() {
    }

    public Date getDataRegistro() {
        return dataRegistro;
    }

    public void setDataRegistro(Date dataRegistro) {
        this.dataRegistro = dataRegistro;
    }

    public TipoRegistroEnum getTipoBatida() {
        return tipoBatida;
    }

    public void setTipoBatida(TipoRegistroEnum tipoBatida) {
        this.tipoBatida = tipoBatida;
    }
}
