package br.com.apipontoeletronico.controller;

import br.com.apipontoeletronico.models.Usuario;
import br.com.apipontoeletronico.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @PostMapping
    public ResponseEntity<Usuario> cadastrarUsuario(@RequestBody @Valid Usuario usuario){
        try {
            Usuario usuarioRetorno = usuarioService.cadastrarUsuario(usuario);
            return ResponseEntity.status(201).body(usuarioRetorno);
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Usuario editarUsuario(@PathVariable(name = "id") long id, @RequestBody @Valid Usuario usuario){
        try {
            return usuarioService.editarUsuario(id,usuario);
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
    @GetMapping("/{id}")
    public Usuario consultarUsuario(@PathVariable(name = "id") long id){
        try {
            return usuarioService.consultarUsuario(id);
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
    @GetMapping
    public Iterable<Usuario> consultarTodosUsuarios(){
        try{
            return usuarioService.consultarTodosUsuarios();
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

}
