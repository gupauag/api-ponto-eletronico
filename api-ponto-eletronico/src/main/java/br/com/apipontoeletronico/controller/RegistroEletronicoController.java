package br.com.apipontoeletronico.controller;

import br.com.apipontoeletronico.enums.TipoRegistroEnum;
import br.com.apipontoeletronico.models.DTO.PontoBatidoDTO;
import br.com.apipontoeletronico.models.RegistroEletronico;
import br.com.apipontoeletronico.service.RegistroEletronicoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/registrosEletronico")
public class RegistroEletronicoController {

    @Autowired
    private RegistroEletronicoService registroEletronicoService;

    @PostMapping("/{id}")
    public ResponseEntity<RegistroEletronico> cadastrarPontoEletronico(@PathVariable(name = "id") long id,
                                                                       @RequestParam(name = "tipoRegistro")
                                                            TipoRegistroEnum tipoRegistroEnum){
        try {
            RegistroEletronico registroEletronico = registroEletronicoService.cadastrarPontoEletronico(id, tipoRegistroEnum);
            return ResponseEntity.status(201).body(registroEletronico);
            //return registroEletronicoService.cadastrarPontoEletronico(id, tipoRegistroEnum);
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public PontoBatidoDTO listarPontosBatidosDoDia(@PathVariable(name = "id") long id){
        try {
            return registroEletronicoService.listarPontosBatidosDoDia(id);
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

}
