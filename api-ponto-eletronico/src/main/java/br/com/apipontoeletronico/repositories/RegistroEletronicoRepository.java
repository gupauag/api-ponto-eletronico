package br.com.apipontoeletronico.repositories;

import br.com.apipontoeletronico.models.RegistroEletronico;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RegistroEletronicoRepository extends CrudRepository<RegistroEletronico, Long> {

    @Query(value = "select tipo_batida from registro_eletronico " +
            "where usuario_id = :id_usuario " +
            "and data_registro = (select max(data_registro) " +
            "from registro_eletronico " +
            "where usuario_id = :id_usuario " +
            "and date(data_registro) = curdate())", nativeQuery = true)
    String ultimoResgistroPontoDoDia(@Param("id_usuario") long id_usuario);

    @Query(value = "select * from registro_eletronico " +
            "where usuario_id = :id_usuario " +
            "and date(data_registro) = curdate()", nativeQuery = true)
    List<RegistroEletronico> getTodosRegistrosDoDia(@Param("id_usuario") long usuario_id);

    List<RegistroEletronico> findByUsuarioId(long id_usuario);

}