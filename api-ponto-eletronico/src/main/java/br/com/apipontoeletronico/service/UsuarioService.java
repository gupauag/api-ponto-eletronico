package br.com.apipontoeletronico.service;

import br.com.apipontoeletronico.models.Usuario;
import br.com.apipontoeletronico.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    public Usuario cadastrarUsuario(Usuario usuario){
        Optional<Usuario> usuarioRetorno = usuarioRepository.findByEmail(usuario.getEmail());
        if(!usuarioRetorno.isPresent()){
            usuario.setDataCadastro(LocalDate.now());
            return usuarioRepository.save(usuario);
        }else {
            throw new RuntimeException("Email já cadastrado na base!");
        }

    }

    public Usuario editarUsuario(long id, Usuario usuario){
        Optional<Usuario> usuarioOptional = usuarioRepository.findById(id);
        if(usuarioOptional.isPresent()){
            return usuarioRepository.save(usuario);
        }else {
            throw new RuntimeException("Usuario não encontrado");
        }
    }

    public Usuario consultarUsuario(long id){
        Optional<Usuario> usuarioOptional = usuarioRepository.findById(id);
        if(usuarioOptional.isPresent()){
            return usuarioOptional.get();
        }else {
            throw new RuntimeException("Usuario não encontrado");
        }
    }

    public Iterable<Usuario> consultarTodosUsuarios(){
       try {
           return usuarioRepository.findAll();
       }catch (Exception e){
           throw new RuntimeException("Não foi possível achar usuarios");
       }
    }


}
