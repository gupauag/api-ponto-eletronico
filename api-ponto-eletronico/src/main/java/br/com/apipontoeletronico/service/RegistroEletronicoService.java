package br.com.apipontoeletronico.service;

import br.com.apipontoeletronico.enums.TipoRegistroEnum;
import br.com.apipontoeletronico.models.DTO.PontoBatidoDTO;
import br.com.apipontoeletronico.models.DTO.TipoRegistroDTO;
import br.com.apipontoeletronico.models.RegistroEletronico;
import br.com.apipontoeletronico.models.Usuario;
import br.com.apipontoeletronico.repositories.RegistroEletronicoRepository;
import br.com.apipontoeletronico.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class RegistroEletronicoService {

    @Autowired
    private RegistroEletronicoRepository registroEletronicoRepository;

    @Autowired
    private UsuarioRepository usuarioRepository;

    public RegistroEletronico cadastrarPontoEletronico(long idUsuario, TipoRegistroEnum tipoRegistroEnum){
        Optional<Usuario> usuarioOptional = usuarioRepository.findById(idUsuario);
        if(usuarioOptional.isPresent()){
            Date dataRegistro = new Date();
            RegistroEletronico registroEletronico = new RegistroEletronico();

            String ultimoRegistro = registroEletronicoRepository.ultimoResgistroPontoDoDia(idUsuario);
            if(ultimoRegistro == null || ultimoRegistro.equals("S")) {
                if (tipoRegistroEnum.name().equals("S")) throw new RuntimeException("Já existe uma marcação de " +
                        "SAÍDA ativa. Verifique o tipo de entrada ou ajuste as marcações antes de submeter novamente");
                registroEletronico.setTipoBatida(TipoRegistroEnum.E);
            }
            else{
                if (tipoRegistroEnum.name().equals("E")) throw new RuntimeException("Já existe uma marcação de " +
                        "ENTRADA ativa. Verifique o tipo de entrada ou ajuste as marcações antes de submeter novamente");
                registroEletronico.setTipoBatida(TipoRegistroEnum.S);
            }

            registroEletronico.setDataRegistro(dataRegistro);
            registroEletronico.setUsuario(usuarioOptional.get());

            return registroEletronicoRepository.save(registroEletronico);
        }else throw new RuntimeException("Usuario inexistente!");

    }

    public PontoBatidoDTO listarPontosBatidosDoDia(long id_usuario){
        List<RegistroEletronico> registrosEletronico = registroEletronicoRepository.findByUsuarioId(id_usuario);
        if(!registrosEletronico.isEmpty()){

            Iterable<RegistroEletronico> registrosUsuario =
                    registroEletronicoRepository.getTodosRegistrosDoDia(id_usuario);
            List<TipoRegistroDTO> listaTipoRegistroDTOS = new ArrayList<>();

            TipoRegistroDTO tipoRegistroDTO;
            for(RegistroEletronico ponto : registrosUsuario){
                tipoRegistroDTO = new TipoRegistroDTO();
                tipoRegistroDTO.setDataRegistro(ponto.getDataRegistro());
                tipoRegistroDTO.setTipoBatida(ponto.getTipoBatida());
                listaTipoRegistroDTOS.add(tipoRegistroDTO);
            }

            PontoBatidoDTO pontoBatidoDTO = new PontoBatidoDTO();
            pontoBatidoDTO.setRegistrosPonto(listaTipoRegistroDTOS);
            pontoBatidoDTO.setTotalTrabalhado(calculaHorasTrabalhadas(listaTipoRegistroDTOS));

            return pontoBatidoDTO;
        }else throw new RuntimeException("Usuario sem registro de ponto!");
    }

    public String calculaHorasTrabalhadas(List<TipoRegistroDTO> listaTipoRegistroDTOS){
        Date dataAtual = new Date();
        long milisecondEntrada = 0;
        long milisecondSaida = 0;

        for(TipoRegistroDTO registro: listaTipoRegistroDTOS){
            if(registro.getTipoBatida().name().equals("E")){
                milisecondEntrada += registro.getDataRegistro().getTime();
            } else {
                milisecondSaida += registro.getDataRegistro().getTime();
            }
        }
        //Marcação ainda esta em aberto!!
        if(listaTipoRegistroDTOS.size()%2 != 0){
            milisecondSaida += dataAtual.getTime();
        }

        long milisecondResultado = (milisecondSaida - milisecondEntrada);
        int sec = (int) (milisecondResultado / 1000) % 60 ;
        int min = (int) ((milisecondResultado / (1000*60)) % 60);
        int horas   = (int) ((milisecondResultado / (1000*60*60)) % 24);

        String totalTrabalho = horas + ":"+min+ ":" +sec;

        return totalTrabalho;
    }

}
