# api-ponto-eletronico

# Configuração - Banco de dados
- Scripts MySQL - MariaDB:

create database ponto_eletronico;

create user 'admin'@'localhost' identified by 'admin';

use ponto_eletronico;

grant all on ponto_eletronico.* to 'admin'@'localhost'; - grant all

# Confuguração application.properties

spring.datasource.url=jdbc:mysql://localhost:3306/ponto_eletronico

spring.datasource.username=admin

spring.datasource.password=admin

spring.jpa.properties.hibernate.dialect = org.hibernate.dialect.MySQL5InnoDBDialect

spring.jpa.hibernate.ddl-auto=update

spring.jpa.show-sql=true

# Documentação ENDPOITS API
Usuario

- Validação de campos usuario:
    - nomeCompleto 
        - O nome deve ter entre 3 e 255 caracteres
        - Inclusão do nome é obrigatória 
    - cpf
        - CPF tem que ser válido
        - Inclusão do CPF é obrigatório
    - email
        - Formato do email tem que ser válido
        - Inclusão do email é obrigatória

- Cadastrar Usuario - método POST:
    - rota: http://localhost:8080/usuarios
    - Body: 
        {
         "nomeCompleto": "Nome teste 1",
         "cpf": "163.309.220-80",
         "email": "teste1@teste.com"
        }
    - Exceções: Não é permitido incluir dois usuarios com emails iguais!
- Editar Usuario - método PUT
    - rota: http://localhost:8080/usuarios/1 (1 é o id que será atualizado)
    - Body: 
        {
         "nomeCompleto": "Nome teste - alterado",
         "cpf": "163.309.220-80",
         "email": "teste1@teste.com"
        }
    - Exceções: Usuario precisa esta cadastrado para ser alterado!
- Consultar Usuario - método GET
    - rota: http://localhost:8080/usuarios/1 (1 é o id que será consultado)
        - Body: {}
        - Exceções: Usuario precisa estar cadastrado para ser consultado!
- Listar Todos Usuarios - método GET
    - rota: http://localhost:8080/usuarios
        - Body: {}
        - Exceções: Precisa haver pelo menos um usuario!

Registro de Ponto
    - Premissas:
        - Usuario precisa estar cadastrado;
        - Não é permitido incluir duas entradas ou saídas seguidas. Ou seja, o sistema não prevê esquecimento de ponto!
        - A cada dia o sistema zera o ponto e tempo de trabalho, ou seja, só contabiliza o total trabalhado por DIA!
            - Foi considerada essa regra, para não ter problemsa de calculo do total trabalhado.
        - Funcionalidade de ajuste de ponto, não foi construída e estará atrelada a V2.
- Cadastrar Ponto - método POST    
    - rota: http://localhost:8080/registrosEletronico/1?tipoRegistro=S (1 é o id que será consultado)
        - Parâmetro: tipoRegistro = S
        - Exceções possíveis: 
            - Usuario precisa estar cadastrado para registrar o ponto!
            - Não é possível cadastrar duas Saídas seguidas:
                - Já existe uma marcação de SAÍDA ativa. Verifique o tipo de entrada ou ajuste as marcações antes de 
                submeter novamente
            - Não é possível cadastrar duas Entradas seguidas:
                - Já existe uma marcação de ENTRADA ativa. Verifique o tipo de entrada ou ajuste as marcações antes de 
                submeter novamente
- Listar Pontos Batidos do Dia - Método GET
    - rota: http://localhost:8080/registrosEletronico/1 (1 é o id do usuário)
            - Exceções possíveis: 
                - Usuario precisa ter pelo menos um registro de ponto!
                - Não é possível cadastrar duas Saídas seguidas:
                    - Já existe uma marcação de SAÍDA ativa. Verifique o tipo de entrada ou ajuste as marcações antes de 
                    submeter novamente
                - Não é possível cadastrar duas Entradas seguidas:
                    - Já existe uma marcação de ENTRADA ativa. Verifique o tipo de entrada ou ajuste as marcações antes de 
                    submeter novamente